﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Prototype_AccessDataBase
{
    public partial class frmHome : Form
    {
        public frmHome()
        {
            InitializeComponent();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            // Change the username, password and database according to your needs
            // You can ignore the database option if you want to access all of them.
            // 127.0.0.1 stands for localhost and the default port to connect.
            string table;
            table = txtbTable.Text;
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=root;database=test;";
            ////
            string query = "use test;";

            // Prepare the connection
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;

            // Let's do it !
            try
            {
                query += "SELECT * FROM " + table + "; ";
                databaseConnection = new MySqlConnection(connectionString);
                commandDatabase = new MySqlCommand(query, databaseConnection);
                commandDatabase.CommandTimeout = 60;

                
                // Open the database
                databaseConnection.Open();

                // Execute the query
                reader = commandDatabase.ExecuteReader();

                // All succesfully executed, now do something

                // IMPORTANT : 
                // If your query returns result, use the following processor :

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        btnConnect.Enabled = false;
                        txtbTable.Enabled = false;

                        btnDisconnect.Enabled = true;

                        // As our database, the array will contain : ID 0, FIRST_NAME 1,LAST_NAME 2, ADDRESS 3
                        // Do something with every received database ROW
                        string[] row = { reader.GetString(0), reader.GetString(1), reader.GetString(2) };
                        pnlStatus.BackColor = Color.LightGreen;
                        listBox1.Items.Add(row[0]);
                        listBox1.Items.Add(row[1]);
                        listBox1.Items.Add(row[2]);
                        listBox1.Items.Add("---------------------");

                    }
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }

                // Finally close the connection
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                // Show any error message.
                MessageBox.Show(ex.Message);
            }

        }

        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            btnDisconnect.Enabled = false;
            btnConnect.Enabled = true;
            txtbTable.Enabled = true;

            pnlStatus.BackColor = Color.Red;

        }

        private void frmHome_Load(object sender, EventArgs e)
        {
            btnDisconnect.Enabled = false;

        }
    }
}
