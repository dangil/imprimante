-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 30 Octobre 2019 à 14:22
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `db_imprimantes`
--

drop database if exists P_042_Imprimantes;
create database P_042_Imprimantes;
use P_042_Imprimantes;

-- --------------------------------------------------------

--
-- Structure de la table `fournir`
--

CREATE TABLE `fournir` (
  `idConstructeur` int(11) NOT NULL,
  `idFournisseur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_cartouche`
--

CREATE TABLE `t_cartouche` (
  `idCartouche` int(11) NOT NULL,
  `carNom` varchar(50) NOT NULL,
  `carType` varchar(50) NOT NULL,
  `carPrix` float NOT NULL DEFAULT '0',
  `idMarque` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_constructeur`
--

CREATE TABLE `t_constructeur` (
  `idConstructeur` int(11) NOT NULL,
  `conNom` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_fournisseur`
--

CREATE TABLE `t_fournisseur` (
  `idFournisseur` int(11) NOT NULL,
  `fouNom` varchar(50) NOT NULL,
  `fouSite` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_imprimante`
--

CREATE TABLE `t_imprimante` (
  `idImprimante` int(11) NOT NULL,
  `impModele` varchar(100) NOT NULL,
  `impPrixActuel` float NOT NULL DEFAULT '000.00',
  `impPrixSortie` float NOT NULL DEFAULT '000.00',
  `impPoids` float NOT NULL DEFAULT '00.0',
  `impLargeur` float NOT NULL DEFAULT '00.0',
  `impLongueur` float NOT NULL DEFAULT '00.0',
  `impHauteur` float NOT NULL DEFAULT '00.0',
  `impVitesseImpression` tinyint(3) NOT NULL,
  `impResolution` varchar(100) NOT NULL,
  `impRectoVerso` tinyint(1) NOT NULL,
  `idCartouche` int(11) DEFAULT NULL,
  `idMarque` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_marque`
--

CREATE TABLE `t_marque` (
  `idMarque` int(11) NOT NULL,
  `idConstructeur` int(11) NOT NULL,
  `marNom` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `fournir`
--
ALTER TABLE `fournir`
  ADD PRIMARY KEY (`idConstructeur`,`idFournisseur`),
  ADD UNIQUE KEY `ID_Fournir_IND` (`idConstructeur`,`idFournisseur`),
  ADD KEY `FKFou_t_f_IND` (`idFournisseur`);

--
-- Index pour la table `t_cartouche`
--
ALTER TABLE `t_cartouche`
  ADD PRIMARY KEY (`idCartouche`),
  ADD UNIQUE KEY `ID_t_cartouche_IND` (`idCartouche`),
  ADD KEY `FKvendre1_IND` (`idMarque`);

--
-- Index pour la table `t_constructeur`
--
ALTER TABLE `t_constructeur`
  ADD PRIMARY KEY (`idConstructeur`),
  ADD UNIQUE KEY `ID_t_Constructeur_IND` (`idConstructeur`);

--
-- Index pour la table `t_fournisseur`
--
ALTER TABLE `t_fournisseur`
  ADD PRIMARY KEY (`idFournisseur`),
  ADD UNIQUE KEY `ID_t_fournisseur_IND` (`idFournisseur`);

--
-- Index pour la table `t_imprimante`
--
ALTER TABLE `t_imprimante`
  ADD PRIMARY KEY (`idImprimante`),
  ADD UNIQUE KEY `ID_t_imprimante_IND` (`idImprimante`),
  ADD KEY `FKutiliser_IND` (`idCartouche`),
  ADD KEY `FKvendre_IND` (`idMarque`);

--
-- Index pour la table `t_marque`
--
ALTER TABLE `t_marque`
  ADD PRIMARY KEY (`idMarque`),
  ADD UNIQUE KEY `FKConstruit_Pour_ID` (`idConstructeur`),
  ADD UNIQUE KEY `ID_t_marque_IND` (`idMarque`),
  ADD UNIQUE KEY `FKConstruit_Pour_IND` (`idConstructeur`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_cartouche`
--
ALTER TABLE `t_cartouche`
  MODIFY `idCartouche` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_constructeur`
--
ALTER TABLE `t_constructeur`
  MODIFY `idConstructeur` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_fournisseur`
--
ALTER TABLE `t_fournisseur`
  MODIFY `idFournisseur` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_imprimante`
--
ALTER TABLE `t_imprimante`
  MODIFY `idImprimante` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_marque`
--
ALTER TABLE `t_marque`
  MODIFY `idMarque` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `fournir`
--
ALTER TABLE `fournir`
  ADD CONSTRAINT `FKFou_t_C` FOREIGN KEY (`idConstructeur`) REFERENCES `t_constructeur` (`idConstructeur`),
  ADD CONSTRAINT `FKFou_t_f_FK` FOREIGN KEY (`idFournisseur`) REFERENCES `t_fournisseur` (`idFournisseur`);

--
-- Contraintes pour la table `t_cartouche`
--
ALTER TABLE `t_cartouche`
  ADD CONSTRAINT `FKvendre1_FK` FOREIGN KEY (`idMarque`) REFERENCES `t_marque` (`idMarque`);

--
-- Contraintes pour la table `t_imprimante`
--
ALTER TABLE `t_imprimante`
  ADD CONSTRAINT `FKutiliser_FK` FOREIGN KEY (`idCartouche`) REFERENCES `t_cartouche` (`idCartouche`),
  ADD CONSTRAINT `FKvendre_FK` FOREIGN KEY (`idMarque`) REFERENCES `t_marque` (`idMarque`);

--
-- Contraintes pour la table `t_marque`
--
ALTER TABLE `t_marque`
  ADD CONSTRAINT `FKConstruit_Pour_FK` FOREIGN KEY (`idConstructeur`) REFERENCES `t_constructeur` (`idConstructeur`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
